/**
 * 
 */
/**
 * @author Patrik
 *
 */
module gitlabtest {
	exports main.app;
	exports main.core;

	requires java.sql;
	requires javafx.base;
	requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.controls;
	requires com.jfoenix;
	requires junit;
}