package main.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DatabaseManager {
	private Connection connection;
	
	
	public void createConnection() {
		try {
			// Load MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			// Establish connection to d
			String conString = "jdbc:mysql://localhost/testdb?serverTimezone=UTC";  
			connection = DriverManager.getConnection(conString, "root", "root");
			System.out.println("Connection successful.");
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Connection failed.");
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		DatabaseManager dbm = new DatabaseManager();
		dbm.createConnection();
	}
	
}
